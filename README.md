# Knowage

## General
Knowage is the professional open source suite for modern business analytics over traditional sources and big data systems. Knowage is the new brand for SpagoBI project: this new brand marks the value of the well-known open source business intelligence suite after significant functional and technological transformations and a new offering model. The suite is composed of several modules, each one conceived for a specific analytical domain. They can be used individually as complete solution for a certain task, or combined with one another to ensure full coverage of user’ requirements.

Original documentation can be found here:  https://knowage.readthedocs.io/en/latest/


## Usage of Knowage
Currently, Knowage is not utilized at all, but we provide an example of how to use within the [Installation How-To's](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/master/howtos/KnowageConfig.md).

Official documentation has instruction for both system admins and users: https://knowage.readthedocs.io/user/JS/README/index.html


## Versioning  
Tested on:  
Version knowagelabs/knowage-server-docker:7.1 (DockerHub digest: sha256:ccced1d1ce72d1b9eee20ae171a4930b6f38e386839bc150547e28f94898583a)  
Version profirator/knowage-server-docker:7.1.1 (DockerHub digest: sha256:7d7a674ebfba8d0ad7a3e980710ed79650787738acec87915bc8eb56027749e5)

## Volume Usage
This component does not use persistent volumes.

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Knowage UI can be accessed via: https://knowage.fiware.opendata-CITY.de/knowage/

## Access the UI
The default user is "biadmin"/"biadmin".
After configuration, a Keyrock-created user is used.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KNOWAGE_DEV_DOMAIN - This variable contains the domain name used for the component in dev-stage.  
KNOWAGE_PROD_DOMAIN - This variable contains the domain name used for the component in production-stage.  
KNOWAGE_STAGING_DOMAIN - This variable contains the domain name used for the component in staging-stage.  
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
### Kubernetes Secrets
Create kubernetes-secret for Knowage  
```
sudo kubectl create secret generic knowage-secret \
	--from-literal=hmacKey=randomKey \
	--from-literal=keyrockClientId=CLIENT_ID \
	--from-literal=keyrockSecret=CLIENT_SECRET \
	--from-literal=keyrockRolesPath=CLIENT_ID/roles \
	--from-literal=keyrockApplicationId=CLIENT_ID \
	--from-literal=keyrockAdminId=admin \
	--from-literal=keyrockAdminEmail=KEYROCK_ADMIN_EMAIL \
	--from-literal=keyrockAdminPassword=KEYROCK_ADMIN_PASSWORD \
	--namespace=fiware-dev|fiware-staging|fiware-prod
```  

Kubernetes-secret for MYSQL database credentials, provided by MYSQL service provider.  
Kubernetes secret `db-secret` should include keys and values to match:  
`USER` - database username  
`PASSWORD` - database password for that user  

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
